<!-- Footer -->
<footer class="page-footer font-small elegant-color darken-3">

    <!-- Footer Elements -->
    <div class="container">

      <!-- Grid row-->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-12 py-5">
          <div class="mb-5 flex-center">

            <!-- Facebook -->
            <a class="fb-ic">
            <img src="<?= base_url('img/facebook.png') ?>" style="width:45px; height:45px" alt="brand">
            </a>
            <!-- Twitter -->
            <a class="tw-ic">
            <img src="<?= base_url('img/twiter.png') ?>" style="width:45px; height:45px" alt="brand">
            </a>
            <!-- Google +-->
            <a class="gplus-ic">
            <img src="<?= base_url('img/google.png') ?>" style="width:45px; height:45px" alt="brand">
            </a>
            <!--Linkedin -->
            <a class="li-ic">
            <img src="<?= base_url('img/linkedin.png') ?>" style="width:45px; height:45px" alt="brand">
            </a>
          </div>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->

    </div>
    <!-- Footer Elements -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
      <a href="https://mdbootstrap.com/education/bootstrap/">Joaquim Benoni</a>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->